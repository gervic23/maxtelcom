Used to be a small contact center (BPO) Formed by my deceased cousin. The website content was incomplete but I used this as part of my portfolio. Powered by WordPress and jQuery.

This source code is not intended to publish online. Please do not upload this source code to a webhost, or share to anyone. I only use this as a part of my portfolio.

======== INSTALLATION ========

Clone this repository to your web directory (htdocs, public_html).

== FOR WINDOWS XAMPP ==

* Copy and pase this tag to C:\xampp\apache\conf\extra\httpd-vhosts.conf

<VirtualHost *:80>
    ServerAdmin webmaster@dummy-host.example.com
    DocumentRoot "C:/xampp/htdocs/maxtelcom"
    ServerName maxtelcom.dev
    ServerAlias www.maxtelcom.dev
</VirtualHost>

* Add this code to C:\WINDOWS\SYSTEM32\Drivers\etc\hosts

127.0.01	maxtelcom.dev

* Create a database "maxtelcom".
* Import maxtelcom.sql to the database.
* Click wp_options table and look for option_name: site_url and home. Change option value to http://maxtelcom.dev
* Open wp-config.php and change database credentials.
* Restart apache.

== For Linux LAMP ==

* Run this command to your command terminal...

sudo gedit /etc/apache2/sites-available/000-default.conf

* Copy and paste this tag...

<VirtualHost *:80>
    ServerAdmin webmaster@dummy-host.example.com
    DocumentRoot "/var/www/maxtelcom"
    ServerName maxtelcom.dev
    ServerAlias www.maxtelcom.dev
</VirtualHost>

* Run this command to your command terminal...

sudo gedit /etc/hosts

* Add this code to your hosts file.

127.0.01	maxtelcom.dev

* Create a database "maxtelcom".
* Import maxtelcom.sql to the database.
* Click wp_options table and look for option_name: site_url and home. Change option value to http://maxtelcom.dev
* Open wp-config.php and change database credentials.
* Restart apache by this command.

sudo systemctl restart apache2


== SYSTEM REQUIREMENTS ==
* PHP 5.4 or higher

== Admin Priviledge ==

http://maxtelcom.dev/wp-admin

Username: admin
Password: onegervic23

