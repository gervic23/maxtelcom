<?php get_header(); ?>

<section class="mainSection">
	<div class="error404Container">
		<img alt="404" src="/wp-content/themes/maxtelcom/img/404.png" />
	</div>
</section>
<div class="clear"></div>

<script type="text/javascript">
	function setCookie(page) {
		var d = new Date();
		d.setTime(d.getTime() + (10 * 1000));
		var expires = "expires=" + d.toUTCString();
		document.cookie = "page" + "=" + page + "; " + expires + ";" + "path=/";

		location = "/";
	}
</script>

<?php get_footer(); ?>