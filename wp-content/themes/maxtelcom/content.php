<!-- Start WP Content -->
<div class="wp-content-container">
	<section class="mainSection">
		<div class="blog-animate-list">
			<?php if (have_posts()): ?>
				<div class="mainPagination">
					<?php if (function_exists("pagination")) {
					    pagination($additional_loop->max_num_pages);
					} ?>
				</div>
				<?php while (have_posts()): the_post(); ?>
					<?php
						$categories = get_the_category();
						$output = '';

						if ($categories) {
							foreach ($categories as $category) {
								$output .= '<a href="'.get_category_link($category->term_id).'">'.$category->cat_name.'</a>, ';
							}

							$output = rtrim($output, ', ');
						}
					?>

					<article class="mainArticle">
						<div class="articleLeft">
							<div class="articleLeft-contents">
								<div class="articleThumbnail"><?php if (has_post_thumbnail()): ?><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a><?php endif; ?></div>
							</div>
						</div>
						<div class="articleRight">
							<div class="articleRight-contents">
								<div class="articleDate"><?php the_time('F j Y g:i:a') ?></div>
								<div class="articleAuthor">Posted By: <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?></a></div>
								<div class="articleCategory"><?php echo $output; ?></div>
								<div class="articleTitle">
									<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
								</div>
								<div class="articleMinContent">
									<?php the_excerpt(); ?>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</article>
				<?php endwhile; ?>
				<div class="mainPagination">
					<?php if (function_exists("pagination")) {
					    pagination($additional_loop->max_num_pages);
					} ?>
				</div>
			<?php else: ?>
				&nbsp;
			<?php endif; ?>
		</div>
	</section>
	<aside class="mainAside">
		<?php require_once('includes/side.php'); ?>
	</aside>
	<div class="clear"></div>
</div>

<?php if (!is_page() && !is_single()): ?>

<script type="text/javascript">
	var $ = jQuery;

	winresize();

	$(window).resize(function() {
		winresize();
	});

	$(".blog-animate-list").animate({
		marginTop: 0,
		opacity: 1
	}, 1500);

	$(".blog-animate-list a").on("click", function() {
		$(".blog-animate-list").animate({
			marginTop: "96%",
			opacity: 0
		}, 1500);
	});
	
	function winresize() {
		if ($(window).width() > 800) {
			var h = $(window).height() - 120;
		} else if ($(window).width() <= 800) {
			var h = $(window).height() - 60;
		}

		$("body").css({
			"background-image": "url(/wp-content/themes/maxtelcom/img/blog-bg.jpg)",
			"background-repeat": "no-repeat",
			"background-size": $(window).width() + "px " + $(window).height() + "px",
			"background-attachment": "fixed"
		});

		$(".wp-content-container").css({
			"height": h + "px",
			"overflow-y": "hidden"
		});

		$(".blog-animate-list").slimscroll({
		  	height:  h + "px",
		  	color: "#999797",
		  	alwaysVisible: true
		});
	}

	function setCookie(page) {
		var d = new Date();
		d.setTime(d.getTime() + (10 * 1000));
		var expires = "expires=" + d.toUTCString();
		document.cookie = "page" + "=" + page + "; " + expires + ";" + "path=/";

		location = "/";
	}
</script>

<?php endif; ?>

<!-- End WP Content -->