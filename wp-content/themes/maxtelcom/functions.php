<?php

if (is_single()) {
	remove_filter( 'the_content', 'wpautop' );
	remove_filter( 'the_excerpt', 'wpautop' );
}

function wpincludes() {
	wp_enqueue_style('main-style', get_template_directory_uri().'/style.css');
	wp_enqueue_style('clouds', get_template_directory_uri().'/css/clouds.css');
	wp_enqueue_script('jquery');
	wp_enqueue_script('slim-scroll', get_template_directory_uri().'/js/jquery.slimscroll.min.js');
	wp_enqueue_script('textition', get_template_directory_uri().'/js/textition.js');
}
add_action('wp_enqueue_scripts', 'wpincludes');


function theme_setup() {
	$menus = array(
		'header-menu' => __('Header Menu'),
		'footer-menu' => __('Footer Menu')
	);

	register_nav_menus($menus);

	add_theme_support('post-thumbnails');
	add_image_size('small-thumbnail', 180, 120, true);
	add_image_size('banner-image', 920, 210, array('left', 'top'));
}
add_action('after_setup_theme', 'theme_setup');

register_sidebar(array(
    'name' => 'Blog Sidebar',
    'id' => 'blog-sidebar'
));


function wp_custom_excerpt($length) {
	return 20;
}
add_filter( 'excerpt_length', 'wp_custom_excerpt', 999 );

function wp_more_excerpt($more) {
	return sprintf( '...<div class="readMore"><a class="read-more" href="%1$s">%2$s</a></div>',
        get_permalink( get_the_ID() ),
        __( 'Read More', 'textdomain' )
    );
}
add_filter( 'excerpt_more', 'wp_more_excerpt' );

/*Homepage Shortcode*/
function homepage_contents() {
	ob_start();
	require './wp-content/themes/maxtelcom/includes/home.php';
	return ob_get_clean();
}
add_shortcode('home_page', 'homepage_contents');

function pagination($pages = '', $range = 4) {  
     $showitems = ($range * 2)+1;  
 
     global $paged;
     if(empty($paged)) $paged = 1;
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
 
     if(1 != $pages)
     {
         echo "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
             }
         }
 
         if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
         echo "</div>\n";
     }
}


function searchfilter($query) {
    if ($query->is_search && !is_admin() ) {
        $query->set('post_type',array('posts'));
    }
    return $query;
}
add_filter('pre_get_posts','searchfilter');

function admin_tab() {
    global $wpdb;

    $name = 'Contact Us';
    $unread = $wpdb->get_var("SELECT COUNT(*) FROM `wp_contact_form` WHERE `status` = 'unread'");

    if ($unread !== "0") {
        $name .= " <span style='margin-left: 3px; color: #fff; border: 1px solid red; background-color: red; border-radius: 100%; font-size: 11px; padding: 0px 5px;'>".$unread."</span>";
    }

    add_menu_page('Contact Us', $name, 'manage_options', 'contactus_options', 'contactus_admin_page', "", 50);
}
add_action('admin_menu', 'admin_tab');

function admin_page_setup() {
    wp_enqueue_script('jquery');
    wp_enqueue_style('contact-us', get_template_directory_uri().'/css/contactus.css');
}
add_action('admin_enqueue_scripts', 'admin_page_setup');

function contactus_admin_page() {
    ob_start();
    global $wpdb;
    require_once('../wp-content/themes/maxtelcom/includes/contactus-admin.php');
    echo ob_get_clean();
}

function contactus_dashboard_setup() {
    wp_add_dashboard_widget('contactus', 'Contact Us Feeds', 'contactus_dashboard_display');
}

function contactus_dashboard_display() {
    global $wpdb;
    $unread = $wpdb->get_var("SELECT COUNT(*) FROM `wp_contact_form` WHERE `status` = 'unread'");
    $total = $wpdb->get_var("SELECT COUNT(*) FROM `wp_contact_form`");
    ?>

    <div class="contactus-dashboard-container">
        <div class="contactus-dashboard-contents">
            <div class="contactus-dashboard-left">
                <div class="contactus-dashboard-left-top">
                    <?php echo $unread; ?>
                </div>
                <div class="contactus-dashboard-left-bottom">
                    <a href="/wp-admin/admin.php?page=contactus_options">Unread Messages</a>
                </div>
            </div>
            <div class="contactus-dashboard-right">
                <div class="contactus-dashboard-right-top">
                    <?php echo $total; ?>
                </div>
                <div class="contactus-dashboard-right-bottom">
                    <a href="/wp-admin/admin.php?page=contactus_options">Toal Messages</a>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <?php
}
add_action('wp_dashboard_setup', 'contactus_dashboard_setup');

function display_comment_recaptcha() {
    ?>
    <div class="g-recaptcha" data-sitekey="6LcHTh4TAAAAAETY_KIEwZgUcmvWYQepCjnouAlS"></div>
    <input name="submit" class="comment-submit-btn" type="submit" value="Submit Comment">
    <?php
}
add_action('comment_form', 'display_comment_recaptcha');

function verify_comment_captcha($commentdata) {
    if (isset($_POST['g-recaptcha-response'])) {
        $recaptcha = $_POST['g-recaptcha-response'];
        $captcha = new ReCaptcha('6LcHTh4TAAAAAGxplVyOYHJnk0cGDcydxs2zUHsk');
        $resp = $captcha->verify($recaptcha, $_SERVER['REMOTE_ADDR']);

        if ($resp->isSuccess()) {
            return $commentdata;
        } else {
            echo __('Please verify if you are not a robot.');
        }
    } else {
        echo __('Please verify if you are not a robot.');
    }
}
add_filter('preprocess_comment', 'verify_comment_captcha');

function login_recaptcha_script() {
    wp_register_script("recaptcha_login", "https://www.google.com/recaptcha/api.js");
    wp_enqueue_script("recaptcha_login");
}
add_action('login_enqueue_scripts', 'login_recaptcha_script');

function display_login_captcha() {
    ?>
    <div class="g-recaptcha" data-sitekey="6LcHTh4TAAAAAETY_KIEwZgUcmvWYQepCjnouAlS"></div>
    <?php
}
add_action('login_form', 'display_login_captcha');

function verify_login_captcha($user, $password) {
    if (isset($_POST['g-recaptcha-response'])) {
        $recaptcha = $_POST['g-recaptcha-response'];
        $captcha = new ReCaptcha('6LcHTh4TAAAAAGxplVyOYHJnk0cGDcydxs2zUHsk');
        $resp = $captcha->verify($recaptcha, $_SERVER['REMOTE_ADDR']);

        if ($resp->isSuccess()) {
            return $user;
        } else {
            return new WP_Error("Captcha Invalid", __("<strong>ERROR</strong>: Please verify if you are not a robot."));
        }
    } else {
        return new WP_Error("Captcha Invalid", __("<strong>ERROR</strong>: Please verify if you are not a robot."));
    }
}
add_filter('wp_authenticate_user', 'verify_login_captcha', 10, 2);


function ajaxprocess() {
    global $wpdb;
    $operation = $_POST['operation'];

    switch ($operation) {
        case "sendingcontact":
            $name = $_POST['name'];
            $email = $_POST['email'];
            $message = $_POST['message'];
            $recaptcha = $_POST['recaptcha'];

            if (isset($recaptcha)) {
                $captcha = new ReCaptcha('6LcHTh4TAAAAAGxplVyOYHJnk0cGDcydxs2zUHsk');
                $resp = $captcha->verify($recaptcha, $_SERVER['REMOTE_ADDR']);

                if ($resp->isSuccess()) {
                    $wpdb->insert('wp_contact_form', array(
                        'status' => 'unread',
                        'name' => $name,
                        'email' => $email,
                        'message' => $message,
                        'date' => date("Y-m-d H:i:s")
                    ), array('%s', '%s', '%s', '%s', '%s'));

                    echo "ok";
                } else {
                    echo 'recaptcha failed';
                }
            } else {
                 echo 'recaptcha failed';
            }
        break;

        case "delete-contact":
            $id = $_POST['id'];
            $id = explode(',', $id);

            foreach ($id as $d) {
                $wpdb->delete('wp_contact_form', array(
                    'id' => $d
                ), array('%d'));
            }
        break;

        case "delete-single-contact-message":
            $id = $_POST['id'];
            $wpdb->delete('wp_contact_form', array(
                'id' => $id
            ), array('%d'));

            echo 1;
        break;
    }

    wp_die();
}
add_action('wp_ajax_my_action', 'ajaxprocess');
add_action('wp_ajax_nopriv_my_action', 'ajaxprocess');

class ReCaptcha {
    private $secretkey;

    public function __construct($secretkey) {
        $this->secretkey = $secretkey;
    }

    public function verify($publickey, $ip) {
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $full_url = $url.'?secret='.$this->secretkey.'&response='.$publickey.'&ip='.$ip;
        return new ReCapResonse($full_url);
    }
}

class ReCapResonse {
    private $boolean;

    public function __construct($full_url) {
        $data = json_decode(file_get_contents($full_url));

        if (isset($data->success) && $data->success == true) {
            $this->boolean = true;
        } else {
            $this->boolean = false;
        }
    }

    public function isSuccess() {
        return $this->boolean;
    }
}