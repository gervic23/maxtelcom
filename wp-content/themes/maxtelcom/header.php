<!-- Start WP Header -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en-US" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<meta property="fb:app_id" content="1531749127131907" />
	<meta property="fb:admins" content="100010263937297"/>
	<meta property="fb:admins" content="1255829841097130"/>
	<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Bangers' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Aldrich' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Bad+Script' rel='stylesheet' type='text/css'>
   	<?php if (is_page() || is_single()): ?>
	<title><?php the_title(); ?> - <?php bloginfo('name'); ?></title>
	<?php elseif (is_category()): ?>
	<title><?php single_cat_title(); ?> - <?php bloginfo('name'); ?></title>
	<?php elseif (is_tag()): ?>
	<title><?php single_tag_title(); ?> - <?php bloginfo('name'); ?></title>
	<?php elseif (is_author()): ?>
	<title><?php echo get_the_author(); echo '\'s Archives'; ?> - <?php bloginfo('name'); ?></title>
	<?php elseif (is_day()): ?>
	<title><?php echo 'Daily Archives '.get_the_date(); ?> - <?php bloginfo('name'); ?></title>
	<?php elseif (is_month()): ?>
	<title><?php echo 'Monthly Archives '.get_the_date('F Y'); ?> - <?php bloginfo('name'); ?></title>
	<?php elseif (is_year()): ?>
	<title><?php echo 'Yearly Archives '.get_the_date('Y'); ?> - <?php bloginfo('name'); ?></title>
	<?php elseif (is_search()): ?>
	<title><?php echo get_search_query(); ?> - <?php bloginfo('name'); ?></title>
	<?php else: ?>
	<title>Blog - <?php bloginfo('name'); ?></title>
	<?php endif ?>
	<?php if (is_home()): ?>
	<?php endif; ?>
	<?php wp_head(); ?>
	<?php if (is_single() || !is_page()): ?>
	<div id="fb-root"></div>
		<!-- Load Facebook SDK for JavaScript -->
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
	<?php endif; ?>
</head>

<script src='https://www.google.com/recaptcha/api.js'></script>

<body>

<div class="main-container">
	<?php if (!is_page()): ?>
		<header class="main-header">
			<div class="main-header-left">
				<span>MaxTelcom</span>
			</div>
			<div class="main-header-right">
				<ul>
					<li><a href="javascript:" onclick="javascript: setCookie('home')">Home</a></li>
					<li><a href="javascript:" onclick="javascript: setCookie('aboutus')">About Us</a></li>
					<li><a href="javascript:" onclick="javascript: setCookie('services')">Services</a></li>
					<li><a href="javascript:" onclick="javascript: setCookie('joinus')">Join Us</a></li>
					<li><a href="javascript:" onclick="javascript: setCookie('contactus')">Contact Us</a></li>
					<li><a href="/blog">Blog</a></li>
				</ul>
			</div>
			<div class="clear"></div>
		</header>
		<header class="mobile-header">
			<ul>
				<li><span class="icons icon-menu menu-toggle"></span></li>
				<li>
					<div class="mobile-search">
						<form action="/blog" accept-charset="UTF-8" method="GET">
							<input type="text" name="s" autocomplete="off" placeholder="Search Here" />
							<input type="submit" value="search" />
						</form>
					</div>
				</li>
			</ul>
		</header>
	<?php endif; ?>

	<div class="main-contents">
		<?php if (!is_page()): ?>
			<div class="mobile-links">
				<ul>
					<li><a href="javascript:" onclick="javascript: setCookie('home')">Home</a></li>
					<li><a href="javascript:" onclick="javascript: setCookie('aboutus')">About Us</a></li>
					<li><a href="javascript:" onclick="javascript: setCookie('services')">Services</a></li>
					<li><a href="javascript:" onclick="javascript: setCookie('contactus')">Contact Us</a></li>
					<li><a href="/blog">Blog</a></li>
				</ul>
			</div>

			<script type="text/javascript">
				var $ = jQuery;
				var toggle = false;

				fixSize();

				$(window).resize(function() {
					fixSize();
				});

				$(".menu-toggle").on("click", function() {
					if (toggle == false) {
						toggle = true;
						$(".mobile-links").animate({
							left: 0
						}, 500);
					} else {
						toggle = false;
						$(".mobile-links").animate({
							left: "-101%"
						}, 500);
					}
				});

				$(".main-header-left span").on("click", function() {
					location = "/";
				});
				
				function fixSize() {
					var win_height = $(window).height();
					var menu_height = win_height - 46;

					$(".mobile-links").css({
						"height": menu_height
					});
				}
			</script>
		<?php endif; ?>