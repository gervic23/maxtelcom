<?php if (!$_GET['p']): ?>
	<?php
		$count = $wpdb->get_var("SELECT COUNT(*) FROM `wp_contact_form`");
		if ($count !== "0") {
			$messages = $wpdb->get_results("SELECT * FROM `wp_contact_form` ORDER BY `id` DESC");
		}
	?>

	<?php if ($count == "0"): ?>
		<div class="contactus-container">
			There are no contacts at this time.
		</div>
	<?php else: ?>
		<div class="contactus-container">
			<div class="contactus-btn-container">
				<input type="button" id="delete" value="DELETE" />
			</div>
			<table>
				<thead>
					<tr>
						<th><input type="checkbox" class="maincheckbox1" /></th>
						<th>Name</th>
						<th>Email</th>
						<th>Date</th>
						<th>&nbsp;</th>
					</tr>
					<tbody>
						<?php foreach ($messages as $message): ?>
							<?php if ($message->status == 'unread'): ?>
								<tr>
									<th><input type="checkbox" class="ccheckbox" id="id_<?php echo $message->id; ?>" /></th>
									<th><?php echo $message->name; ?></th>
									<th><?php echo $message->email; ?></th>
									<th><?php echo $message->date; ?></th>
									<td><a href="<?php echo site_url(); ?>/wp-admin/admin.php?page=contactus_options&p=read&id=<?php echo $message->id; ?>">Read Message</a></td>
								</tr>
							<?php else: ?>
								<tr>
									<th><input type="checkbox" class="ccheckbox" /></th>
									<td><?php echo $message->name; ?></td>
									<td><?php echo $message->email; ?></td>
									<td><?php echo $message->date; ?></td>
									<td><a href="<?php echo site_url(); ?>/wp-admin/admin.php?page=contactus_options&p=read&id=<?php echo $message->id; ?>">Read Message</a></td>
								</tr>
							<?php endif; ?>
						<?php endforeach; ?>
					</tbody>
				</thead>
			</table>
			<div class="contactus-btn-container">
				<input type="button" id="delete2" value="DELETE" />
			</div>
		</div>
	<?php endif; ?>

	<?php if ($count !== 0): ?>
	<script type="text/javascript">
		var $ = jQuery;
		var checked = false;
		var ccheckbox = $(".ccheckbox");

		$(".maincheckbox1").on("click", function() {
			var ccheckbox = $(".ccheckbox");

			if (checked == false) {
				checked = true;
				for (var i = 0; i < ccheckbox.length; i++) {
					ccheckbox.attr("checked", "checked");
				}
			} else {
				checked = false;
				for (var i = 0; i < ccheckbox.length; i++) {
					ccheckbox.attr("checked", false);
				}
			}
		});

		$("#delete, #delete2").on("click", function() {
			var id = "";

			for (var i = 0; i < ccheckbox.length; i++) {
				if (ccheckbox[i].checked) {
					var id_grab = ccheckbox[i].getAttribute("id");
					id += id_grab.substring(3) + ",";
				}
			}

			id = id.substring(0, id.length - 1);

			var data = {
				"action": "my_action",
				"operation": "delete-contact",
				"id": id
			};

			$.post(ajaxurl, data, function(r) {
				location.reload();
			});
		});
	</script>
	<?php endif; ?>
<?php elseif ($_GET['p'] == 'read' && $_GET['id']): ?>
	<?php
		$id = $_GET['id'];
		$messages = $wpdb->get_results("SELECT * FROM `wp_contact_form` WHERE `id`='$id'");

		if ($messages[0]->status == 'unread') {
			$wpdb->update('wp_contact_form', array(
				'status' => 'readed'
			), array('id' => $id));
		}
	?>

	<div class="contactus-container">
		<div class="contactus-btn-container">
			<input type="button" id="delete" value="DELETE" /> <input type="button" id="goback" value="Go Back" />
		</div>
		<ul>
			<li>Name: <?php echo $messages[0]->name; ?></li>
			<li>Email: <?php echo $messages[0]->email; ?></li>
			<li>Date: <?php echo $messages[0]->date; ?></li>
			<li>Message: <?php echo $messages[0]->message; ?></li>
		</ul>
	</div>

	<script type="text/javascript">
		var $ = jQuery;
		var id = <?php echo $messages[0]->id; ?>

		$("#delete").on("click", function() {
			var c = confirm("Are you sure you want to delete this message?");
			if (c) {
				var data = {
					"action": "my_action",
					"operation": "delete-single-contact-message",
					"id": id
				};

				$.post(ajaxurl, data, function(r) {
					if (r == 1) {
						location = "/wp-admin/admin.php?page=contactus_options";
					}
				});
			}
		});

		$("#goback").on("click", function() {
			location = "/wp-admin/admin.php?page=contactus_options";
		});
	</script>
<?php endif; ?>