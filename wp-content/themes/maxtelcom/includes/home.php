<div class="blackscreen"></div>
<div class="notification-box">
	<div class="notification-header">Error sending message</div>
	<div class="notification-error-message"></div>
	<div class="notification-button-container">
		<input type="button" id="_ok" value="OK" />
	</div>
</div>

<div class="mainNav">
	<div class="mainNav-for-pc">
		<div class="mainNav-left">
			<span>MaxTelcom</span>
		</div>
		<div class="mainNav-right">
			<ul>
				<li><a href="javascript:" onclick="javascript: checkoutHome();">Home</a></li>
				<li><a href="javascript:" onclick="javascript: checkoutAboutUs();">About Us</a></li>
				<li><a href="javascript:" onclick="javascript: checkoutServices();">Services</a></li>
				<li><a href="javascript:" onclick="javascript: checkoutJoinUs()">Join Us</a></li>
				<li><a href="javascript:" onclick="javascript: checkoutContactus()">Contact Us</a></li>
				<li><a href="/blog">Blog</a></li>
			</ul>
		</div>
		<div class="clear"></div>
	</div>
	<div class="mainNav-for-mobile">
		<span class="icons icon-menu menu-toggle"></span>
	</div>
</div>

<div class="mobile-nav-menu">
	<ul class="mobile-nav-menu-ul">
		<li><a href="javascript:" onclick="javascript: checkoutHome();">Home</a></li>
		<li><a href="javascript:" onclick="javascript: checkoutAboutUs();">About Us</a></li>
		<li><a href="javascript:" onclick="javascript: checkoutServices();">Services</a></li>
		<li><a href="javascript:" onclick="javascript: checkoutJoinUs()">Join Us</a></li>
		<li><a href="javascript:" onclick="javascript: checkoutContactus()">Contact Us</a></li>
		<li><a href="/blog">Blog</a></li>
	</ul>
</div>

<div class="mainHome sky">
	<div class="moon"></div>
    <div class="clouds_one"></div>
    <div class="clouds_two"></div>
    <div class="clouds_three"></div>
    <div class="textition-container">
    	<p>Bridging the gap</p>
    	<p>MaxTelcom</p>
    </div>
</div>

<div class="mainAboutUs">
	<div class="aboutUsContents">
		<p>
			MaxTelcom offers a wide range of call center service including both inbound and outbound calls.
			Customer satisfaction is what we aspire for.  Service and empathy is the heart of our business.
		</p>
		<p>Objectives</p>
		<ul class="mainAboutUs-list">
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>
</div>

<div class="mainServices">
	<div class="servicesContents">
		<p>Primary Customers</p>
		<p>
			The company's main clients will be companies that require high amounts of communication between themselves and
			their clients. This includes medical services, and companies that wish to outsource first-level help desk support.
			By focusing on institutions such as these that have special needs, we believe we will be able to better serve our
			clients and produce a superior service that is more effective that other call center firms.
		</p>

		<div class="mainServices-mainList">
			<div class="mainServices-left">
				<ul class="mainServices-list1">
					<li></li>
					<li></li>
					<li></li>
					<li></li>
					<li></li>
					<li></li>
					<li></li>
				</ul>
			</div>
			<div class="mainServices-right">
				<ul class="mainServices-list2">
					<li></li>
					<li></li>
					<li></li>
					<li></li>
					<li></li>
					<li></li>
					<li></li>
					<li></li>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>

<div class="mainJoinUs">
	<div class="mainJoin_container">
		<h2>Fill in the fields.</h2>
		<div class="aplication_form">
			<?php echo do_shortcode( '[shogo_forms id="101"]' ); ?>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
		</div>
	</div>
</div>

<div class="mainContactUs">
	<div class="contactUsContents">
		<div class="mainContactUs-firstContainer">
			<div class="mainContactUs-left">
				<div class="mainContactUs-left-contents">
					<h1>Contact Us</h1>
					<div class="contactus-form">
						<label>Name: <span class="req1">(required)</span></label><br />
						<input type="text" id="_name" placeholder="Your name here..." /><br />
						<label>Email: <span class="req2">(required)</span></label>
						<input type="email" id="_email" placeholder="Your email here..." /><br />
						<label>Message:</label>
						<textarea id="_message"></textarea><br />
						<div class="g-recaptcha" data-sitekey="6LcHTh4TAAAAAETY_KIEwZgUcmvWYQepCjnouAlS"></div>
						<input type="button" id="_send" value="Send" />
						<div class="sent-notification">
							Message Sent
						</div>
					</div>
					<div class="mainContactUs-left-emailUs-container">
						<div class="mainContactUs-left-emailUs-container-header"><h3>OR</h3></div>
						<div class="mainContactUs-left-emailUs-contents">
							Email us at maxtelcomteam@gmail.com
						</div>
					</div>
				</div>

				<script type="text/javascript">
					var $ = jQuery;

					winfixsize();

					$(window).resize(function() {
						winfixsize();
					});

					function winfixsize() {
						var win_height = $(window).height();
						var win_width = $(window).width();
						var bs_height = win_height - 45;

						if (win_width > 800) {
							var notification_top = (win_height / 2) - 160;
							var notification_left = (win_width / 2) - 360;
						} else if (win_width < 800) {
							var notification_top = (win_height / 2) - 160;
							var notification_left = (win_width / 2) - 120;
						}

						$(".blackscreen").css({
							"height": bs_height + "px"
						});

						$(".notification-box").css({
							"top": notification_top + "px",
							"left": notification_left + "px"
						});
					}

					$("#_send").on("click", function() {
						var name = $("#_name").val();
						var email = $("#_email").val();
						var message = $("#_message").val();
						var recaptcha = grecaptcha.getResponse();

						if (name == "") {
							contactus_validation_message("name", "Name is required.");
						} else if (name.length <= 2) {
							contactus_validation_message("name", "Name is too short.");
						} else if (email == "") {
							contactus_validation_message("email", "Email is required.");
						} else if (!email.match(/([a-z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,4})/gi)) {
							contactus_validation_message("email", "Invalid email format.");
						} else if (message == "") {
							contactus_validation_message("message", "Please write your message.");
						} else if (recaptcha == "") {
							contactus_validation_message("", "Please verify if you are not a robot.");
						} else {
							var data = {
								"action": "my_action",
								"operation": "sendingcontact",
								"name": name,
								"email": email,
								"message": message,
								"recaptcha": recaptcha
							}

							$.post("/wp-admin/admin-ajax.php", data, function(r) {
								if (r == "ok") {
									setTimeout(function() {
										$(".sent-notification").fadeIn(500);
										$("#_name, #_email, #_message").attr("disabled", "disabled");
									}, 3000);
								}
							});
						}
					});

					$("#_ok").on("click", function() {
						$(".blackscreen, .notification-box").fadeOut(500);
					});

					$("#_name").on("keyup", function() {
						$(this).css({
							"border": 1 + "px solid #000"
						});

						$(".req1").css({
							"color": "#fff"
						});
					});

					$("#_email").on("keyup", function() {
						$(this).css({
							"border": 1 + "px solid #000"
						});

						$(".req2").css({
							"color": "#fff"
						});
					});

					$("#_message").on("keyup", function() {
						$(this).css({
							"border": 1 + "px solid #000"
						});
					});

					function contactus_validation_message(error, msg) {
						$(".blackscreen, .notification-box").fadeIn(500);
						$(".notification-error-message").html(msg);

						switch (error) {
							case "name":
								$("#_name").css({
									"border": 1 + "px solid red"
								});

								$(".req1").css({
									"color": "red"
								});
							break;

							case "email":
								$("#_email").css({
									"border": 1 + "px solid red"
								});

								$(".req2").css({
									"color": "red"
								});
							break;

							case "message":
								$("#_message").css({
									"border": 1 + "px solid red"
								});
							break;

							default:
								return false;
						}
					}
				</script>

			</div>
			<div class="mainContactUs-right">
				<div class="mainContactUs-right-contents">
					<p>Follow Us</p>
					<ul>
						<li><span class="social-icons fb-icon"></span> <span class="link-str">https://www.facebook.com/maxTelcomteam/</span></li>
						<li><span class="social-icons twitter-icon"></span></li>
						<li><span class="social-icons linkedin-icon"></span></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		<div class="mainFooter">
			All Rights Reserved &copy; <?php echo date("Y"); ?> MaxTelcom.
		</div>
	</div>
</div>

<script type="text/javascript">
	var $ = jQuery;
	var home_scrollTop = 0;
	var aboutus_scrollTop;
	var services_scrollTop;
	var join_us_scrollTop;
	var contactus_scrollTop;

	var aboutus_animate = false;
	var services_animate = false;
	var menu_toggle = false;

	var active_page = "home";

	$("html, body").animate({
		scrollTop: 0
	}, "500");

	$(".mainNav-left span").on("click", function() {
		location = "/";
	});

	winResize();

	$(".aboutUsContents").hide();
	$(".servicesContents").hide();
	$(".contactUsContents").hide();

	$(window).resize(function() {
		winResize();
	});

	$('.textition-container').textition({
		 speed: 1,
		 animation: 'ease-out',
		 map: {x: 200, y: 100, z: 0},
		 autoplay: true,
		 interval: 5
	});

	$(".menu-toggle").on("click", function() {
		if (menu_toggle == false) {
			menu_toggle = true;
			$(".mobile-nav-menu").animate({
				left: 0
			}, 500);
		} else {
			menu_toggle = false;
			$(".mobile-nav-menu").animate({
				left: "-101%"
			}, 500);
		}
	});

	$(".SF_submit_button").on("click", function() {
		$("html, body").scrollTop(join_us_scrollTop);
	});

	$(window).on("scroll", function() {
		if ($(this).scrollTop() >= 0) {
			$("body").css({
				"background-image": "url(/wp-content/themes/maxtelcom/img/cloud_three.png)",
				"background-size": 100 + "%" + 100 + "%",
				"background-attachment": "fixed"
			});
		}

		if ($(this).scrollTop() >= home_scrollTop + 1) {
			$("body").css({
				"background-image": "url(/wp-content/themes/maxtelcom/img/download.png)",
				"background-size": "cover",
				"background-repeat": "repeat",
				"background-attachment": "fixed",
				"background-position": "center center"
			});
		}

		if ($(this).scrollTop() >= aboutus_scrollTop + 1) {
			$("body").css({
				"background-image": "url(/wp-content/themes/maxtelcom/img/bg.jpg)",
				"background-size": "cover",
				"background-repeat": "repeat",
				"background-attachment": "fixed",
				"background-position": "center center"
			});
		}

		if ($(this).scrollTop() >= services_scrollTop + 1) {
			$("body").css({
				"background-color": "#2c2c2c",
				"background-image": "none"
			});
		}
	});

	function winResize() {
		var win_height = $(window).height();

		if (win_height > 800) {
			var half = (win_height / 2) - 45;
			var minus = 0;
			home_scrollTop = 0;
			aboutus_scrollTop = win_height - minus + 45;
			services_scrollTop = (win_height - minus + 1) * 2;
			join_us_scrollTop = (win_height - minus + 1) * 3;
			contactus_scrollTop = (win_height - minus + 1) * 4;
		} else if (win_height < 800) {
			var half = (win_height / 2) - 45;
			var minus = 0;
			home_scrollTop = 110;
			aboutus_scrollTop = win_height - minus + 50;
			services_scrollTop = (win_height - minus + 1) * 2;
			join_us_scrollTop = (win_height - minus + 1) * 3;
			contactus_scrollTop = (win_height - minus + 1) * 4;
		}

		switch (active_page) {
			case "home":
				$("html, body").animate({
					scrollTop: 0
				}, "500");
			break;

			case "aboutus":
				$("html, body").animate({
					scrollTop: aboutus_scrollTop
				}, "500");
			break;

			case "services":
				$("html, body").animate({
					scrollTop: services_scrollTop
				}, "500");
			break;

			case "joinus":
				$("html, body").animate({
					scrollTop: join_us_scrollTop
				}, "500");
			break;

			case "contactus":
				$("html, body").animate({
					scrollTop: contactus_scrollTop
				}, "500");
			break;
		}

		$(".wp-content-container").css({"margin-top": 0});
		$(".mainHome, .sky, .clouds_one, .clouds_two, .clouds_three").css({"height": win_height + "px"});
		$(".textition-container").css({"margin-top": half + "px"});

		$(".mobile-nav-menu").css({"height": win_height - 38 + "px"});
		$(".mainAboutUs").css({"height": win_height - minus + "px"});
		$(".mainServices").css({"height": win_height - minus + "px"});
		$(".mainJoinUs").css({"height": win_height - minus + "px"});
		$(".mainContactUs").css({"height": win_height - minus + "px"});

		$(".mainContactUs").slimscroll({
		  	height: $(".mainContactUs").height() - 50,
		  	color: "#999797"
		});

		$(".mainAboutUs").slimscroll({
		  	height: $(".mainAboutUs").height(),
		  	color: "#b2450a"
		});

		$(".mainServices").slimscroll({
		  	height: $(".mainServices").height(),
		  	color: "#555353"
		});

		$(".mainJoinUs").slimscroll({
	  	height: $(".mainJoinUs").height(),
	  	color: "#555353"
		});
	}

	function checkoutHome() {
		active_page = "home";

		$("html, body").animate({
			scrollTop: 0
		}, "500");

		$(".mainNav").css({
			"background-color": "#0087e5",
			"border-bottom": 1 + "px solid #016bb6",
			"border-top": 1 + "px solid #016bb6"
		});

		$(".mobile-nav-menu ").css({
			"border-top": 1 + "px solid #016bb6",
			"background-color": "#0087e5"
		});

		menu_toggle = false;
		$(".mobile-nav-menu").animate({
			left: "-101%"
		}, 500);

		$("title").html("Home - MaxTelcom");

		$(".aboutUsContents").hide();
		$(".servicesContents").hide();
		$(".contactUsContents").hide();

		$(".mobile-nav-menu").css({
			"background-image": "url(/wp-content/themes/maxtelcom/img/blog-bg.jpg)"
		});
		$(".mobile-nav-menu-ul").css({
			"border": 1 + "px solid #ccc",
			"background-color": "#0087e5"
		});
	}

	function checkoutAboutUs() {
		active_page = "aboutus";

		$("html, body").animate({
			scrollTop: aboutus_scrollTop
		}, "500");

		$(".mainNav").css({
			"background-color": "#ce510d",
			"border-bottom": 1 + "px solid #b2450a",
			"border-top": 1 + "px solid #b2450a",
		});

		aboutUsAnimate();

		menu_toggle = false;
		$(".mobile-nav-menu").animate({
			left: "-101%"
		}, 500);

		$("title").html("About Us - MaxTelcom");

		$(".aboutUsContents").show();
		$(".servicesContents").hide();
		$(".contactUsContents").hide();

		$(".mobile-nav-menu").css({
			"background-image": "url(/wp-content/themes/maxtelcom/img/download.png)"
		});
		$(".mobile-nav-menu-ul").css({
			"border": 1 + "px solid #b2450a",
			"background-color": "#ce510d"
		});
	}

	function checkoutServices() {
		active_page = "services";

		$("html, body").animate({
			scrollTop: services_scrollTop
		}, "500");

		$(".mainNav").css({
			"background-color": "#646161",
			"border-bottom": 1 + "px solid #555353",
			"border-top": 1 + "px solid #555353"
		});

		$(".mobile-nav-menu").css({
			"border-top": 1 + "px solid #555353",
			"background-color": "#646161"
		});

		servicesAnimate();

		menu_toggle = false;
		$(".mobile-nav-menu").animate({
			left: "-101%"
		}, 500);

		$("title").html("Services - MaxTelcom");

		$(".aboutUsContents").hide();
		$(".servicesContents").show();
		$(".contactUsContents").hide();

		$(".mobile-nav-menu").css({
			"background-image": "url(/wp-content/themes/maxtelcom/img/bg.jpg)"
		});
		$(".mobile-nav-menu-ul").css({
			"border": 1 + "px solid #555353",
			"background-color": "#000"
		});
	}

	function checkoutJoinUs() {
		active_page = "joinus";

		$("html, body").animate({
			scrollTop: join_us_scrollTop
		}, "500");

		$(".mainNav").css({
			"background-color": "#646161",
			"border-bottom": 1 + "px solid #555353",
			"border-top": 1 + "px solid #555353"
		});

		$(".mobile-nav-menu").css({
			"border-top": 1 + "px solid #555353",
			"background-color": "#646161"
		});


		menu_toggle = false;
		$(".mobile-nav-menu").animate({
			left: "-101%"
		}, 500);

		$("title").html("Join Us - MaxTelcom");

		$(".aboutUsContents").hide();
		$(".servicesContents").show();
		$(".contactUsContents").hide();

		$(".mobile-nav-menu").css({
			"background-image": "url(/wp-content/themes/maxtelcom/img/bg.jpg)"
		});
		$(".mobile-nav-menu-ul").css({
			"border": 1 + "px solid #555353",
			"background-color": "#000"
		});
	}

	function checkoutContactus() {
		active_page = "contactus";

		$("html, body").animate({
			scrollTop: contactus_scrollTop
		}, "500");

		$(".mainNav").css({
			"background-color": "#7e7d7e",
			"border-bottom": 1 + "px solid #5a5858",
			"border-top": 1 + "px solid #5a5858"
		});

		$(".mobile-nav-menu ").css({
			"border-top": 1 + "px solid #5a5858",
			"background-color": "#7e7d7e"
		});

		menu_toggle = false;
		$(".mobile-nav-menu").animate({
			left: "-101%"
		}, 500);

		$("title").html("Contact Us - MaxTelcom");

		$(".aboutUsContents").hide();
		$(".servicesContents").hide();
		$(".contactUsContents").show();

		$(".mobile-nav-menu").css({
			"background-image": "none",
			"background-color": "#2c2c2c"
		});
		$(".mobile-nav-menu-ul").css({
			"border": 1 + "px solid #555353",
			"background-color": "#000"
		});
	}

	function aboutUsAnimate() {
		if (aboutus_animate == false) {
			aboutus_animate = true;

			var l1 = "We are committed to providing clear and consistent processes that focus on exceeding our clients’ requirements. We continuously measure performance, thereby driving process improvement and customer satisfaction";
			var l2 = "To reduce the number of problems that arise, providing better service from the start.";
			var l3 = "Reducing operating expenses, goes hand-in-hand with improving productivity";
			var l4 = "Improve customer service levels and offer 24*7 support for customers.";
			var l5 = "Establish minimum 95% customer satisfaction rate to form long-term relationships with our clients and create word of mouth marketing/support.";

			var l1_len = l1.length;
			var l2_len = l2.length;
			var l3_len = l3.length;
			var l4_len = l4.length;
			var l5_len = l5.length;

			var speed = 20;
			var c1 = 0;
			var c2 = 0;
			var c3 = 0;
			var c4 = 0;
			var c5 = 0;

			var t1 = setInterval(function() {
				c1++;
				if (c1 !== l1_len) {
					var str = l1.substring(c1, 0);
					$(".mainAboutUs-list li:nth-child(1)").html(str + "_");
				} else {
					clearInterval(t1);
					$(".mainAboutUs-list li:nth-child(1)").html(l1);
					var t2 = setInterval(function() {
						c2++;
						var str = l2.substring(c2, 0);
						if (c2 !== l2_len) {
							$(".mainAboutUs-list li:nth-child(2)").html(str + "_");
						} else {
							clearInterval(t2);
							$(".mainAboutUs-list li:nth-child(2)").html(l2);
							var t3 = setInterval(function() {
								c3++;
								var str = l3.substring(c3, 0);
								if (c3 !== l3_len) {
									$(".mainAboutUs-list li:nth-child(3)").html(str + "_");
								} else {
									clearInterval(t3);
									$(".mainAboutUs-list li:nth-child(3)").html(l3);
									var t4 = setInterval(function() {
										c4++;
										var str = l4.substring(c4, 0);
										if (c4 !== l4_len) {
											$(".mainAboutUs-list li:nth-child(4)").html(str + "_");
										} else {
											clearInterval(t4);
											$(".mainAboutUs-list li:nth-child(4)").html(l4);
											var t5 = setInterval(function() {
												c5++;
												var str = l5.substring(c5, 0);
												if (c5 !== l5_len) {
													$(".mainAboutUs-list li:nth-child(5)").html(str + "_");
												} else {
													clearInterval(t5);
													$(".mainAboutUs-list li:nth-child(5)").html(l5);

												}
											}, speed);
										}
									}, speed);
								}
							}, speed);
						}
					}, speed);
				}
			}, speed);
		}
	}

	function servicesAnimate() {
		if (services_animate == false) {
			services_animate = true;

			var l1 = "Generate sales leads";
			var l2 = "First and Second level help desk Customer Service";
			var l3 = "Database or mailing list information";
			var l4 = "Chat Support/Email Support";
			var l5 = "Web Develop/Web Designing";
			var l6 = "Data Entry Skills";
			var l7 = "Webmaster/Admin Panel";
			var l8 = "Technical Support Tier 1 – Tier 2";
			var l9 = "Billing Support";
			var l10 = "Email Support";
			var l11 = "General Care";
			var l12 = "Research";
			var l13 = "General Virtual Assistant";
			var l14 = "Admin Role";
			var l15 = "Inbound and Outbound";

			var l1_len = l1.length;
			var l2_len = l2.length;
			var l3_len = l3.length;
			var l4_len = l4.length;
			var l5_len = l5.length;
			var l6_len = l6.length;
			var l7_len = l7.length;
			var l8_len = l8.length;
			var l9_len = l9.length;
			var l10_len = l10.length;
			var l11_len = l11.length;
			var l12_len = l12.length;
			var l13_len = l13.length;
			var l14_len = l14.length;
			var l15_len = l15.length;

			speed = 20;

			var c1 = 0;
			var c2 = 0;
			var c3 = 0;
			var c4 = 0;
			var c5 = 0
			var c6 = 0;
			var c7 = 0;
			var c8 = 0;
			var c9 = 0;
			var c10 = 0;
			var c11 = 0;
			var c12 = 0;
			var c13 = 0;
			var c14 = 0;
			var c15 = 0;

			var t = setInterval(function() {
				c1++;
				var str = l1.substring(c1, 0);
				if (c1 !== l1_len) {
					$(".mainServices-list1 li:nth-child(1)").html(str + "_");
				} else {
					clearInterval(t);
					$(".mainServices-list1 li:nth-child(1)").html(l1);
					var t2 = setInterval(function() {
						c2++;
						var str = l2.substring(c2, 0);
						if (c2 !== l2_len) {
							$(".mainServices-list1 li:nth-child(2)").html(str + "_");
						} else {
							clearInterval(t2);
							$(".mainServices-list1 li:nth-child(2)").html(l2);
							var t3 = setInterval(function() {
								c3++;
								var str = l3.substring(c3, 0);
								if (c3 !== l3_len) {
									$(".mainServices-list1 li:nth-child(3)").html(str + "_");
								} else {
									clearInterval(t3);
									$(".mainServices-list1 li:nth-child(3)").html(l3);
									var t4 = setInterval(function() {
										c4++;
										var str = l4.substring(c4, 0);
										if (c4 !== l4_len) {
											$(".mainServices-list1 li:nth-child(4)").html(str + "_");
										} else {
											clearInterval(t4);
											$(".mainServices-list1 li:nth-child(4)").html(l4);
											var t5 = setInterval(function() {
												c5++;
												var str = l5.substring(c5, 0);
												if (c5 !== l5_len) {
													$(".mainServices-list1 li:nth-child(5)").html(str + "_");
												} else {
													clearInterval(t5);
													$(".mainServices-list1 li:nth-child(5)").html(l5);
													var t6 = setInterval(function() {
														c6++;
														var str = l6.substring(c6, 0);
														if (c6 !== l6_len) {
															$(".mainServices-list1 li:nth-child(6)").html(str + "_");
														} else {
															clearInterval(t6);
															$(".mainServices-list1 li:nth-child(6)").html(l6);
															var t7 = setInterval(function() {
																c7++;
																var str = l7.substring(c7, 0);
																if (c7 !== l7_len) {
																	$(".mainServices-list1 li:nth-child(7)").html(str + "_");
																} else {
																	clearInterval(t7);
																	$(".mainServices-list1 li:nth-child(7)").html(l7);
																	var t8 = setInterval(function() {
																		c8++;
																		var str = l8.substring(c8, 0);
																		if (c8 !== l8_len) {
																			$(".mainServices-list2 li:nth-child(1)").html(str + "_");
																		} else {
																			clearInterval(t8);
																			$(".mainServices-list2 li:nth-child(1)").html(l8);
																			var t9 = setInterval(function() {
																				c9++;
																				var str = l9.substring(c9, 0);
																				if (c9 !== l9_len) {
																					$(".mainServices-list2 li:nth-child(2)").html(str + "_");
																				} else {
																					clearInterval(t9);
																					$(".mainServices-list2 li:nth-child(2)").html(l9);
																					var t10 = setInterval(function() {
																						c10++;
																						var str = l10.substring(c10, 0);
																						if (c10 !== l10_len) {
																							$(".mainServices-list2 li:nth-child(3)").html(str + "_");
																						} else {
																							clearInterval(t10);
																							$(".mainServices-list2 li:nth-child(3)").html(l10);
																							var t11 = setInterval(function() {
																								c11++;
																								var str = l11.substring(c11, 0);
																								if (c11 !== l11_len) {
																									$(".mainServices-list2 li:nth-child(4)").html(str + "_");
																								} else {
																									clearInterval(t11);
																									$(".mainServices-list2 li:nth-child(4)").html(l11);
																									var t12 = setInterval(function() {
																										c12++;
																										var str = l12.substring(c12, 0);
																										if (c12 !== l12_len) {
																											$(".mainServices-list2 li:nth-child(5)").html(str + "_");
																										} else {
																											clearInterval(t12);
																											$(".mainServices-list2 li:nth-child(5)").html(l12);
																											var t13 = setInterval(function() {
																												c13++;
																												var str = l13.substring(c13, 0);
																												if (c13 !== l13_len) {
																													$(".mainServices-list2 li:nth-child(6)").html(str + "_");
																												} else {
																													clearInterval(t13);
																													$(".mainServices-list2 li:nth-child(6)").html(l13);
																													var t14 = setInterval(function() {
																														c14++;
																														var str = l14.substring(c14, 0);
																														if (c14 !== l14_len) {
																															$(".mainServices-list2 li:nth-child(7)").html(str + "_");
																														} else {
																															clearInterval(t14);
																															$(".mainServices-list2 li:nth-child(7)").html(l14);
																															var t15 = setInterval(function() {
																																c15++;
																																var str = l15.substring(c15, 0);
																																if (c15 !== l15_len) {
																																	$(".mainServices-list2 li:nth-child(8)").html(str + "_");
																																} else {
																																	clearInterval(t15);
																																	$(".mainServices-list2 li:nth-child(8)").html(l15);
																																}
																															}, speed);
																														}
																													}, speed);
																												}
																											}, speed);
																										}
																									}, speed);
																								}
																							}, speed);
																						}
																					}, speed);
																				}
																			}, speed);
																		}
																	}, speed);
																}
															}, speed);
														}
													}, speed);
												}
											}, speed);
										}
									}, speed);
								}
							}, speed);
						}
					}, speed);
				}
			}, speed);
		}
	}

	function getCookie(cname) {
		if (document.cookie != "") {
			var name = cname + "=";
			var ca = document.cookie.split(";");

			if (ca.length == 1 && ca.length != 0) {
				for (var i = 0; i < ca.length; i++) {
					var c = ca[i];
					while (c.charAt(0) == "") {
						c.substring(1);
					}

					if (c.indexOf(name) == 0) {
						return c.substring(name.length, c.length);
					}
				}
			} else {
				for (var i = 0; i < ca.length; i++) {
					var c = ca[i];
					while (c.charAt(0) == "") {
						c.substring(1);
					}

					if (c.indexOf(name) == 1) {
						return c.substring(name.length + 1, c.length);
					}
				}
			}
		} else {
			return "";
		}
	}

	function checkCookie() {
		var page = getCookie("page");
		if (page != "") {
			switch (page) {
				case "home":
					checkoutHome();
				break;

				case "aboutus":
					checkoutAboutUs();
				break;

				case "services":
					checkoutServices();
				break;

				case "joinus":
					checkoutJoinUs();
				break;

				case "contactus":
					checkoutContactus();
				break;
			}
		} else {
			checkoutHome();
		}
	}

	checkCookie("page");
</script>
