<div class="searchBox">
	<form action="/blog/" accept-charset="UTF-8" method="GET">
		<input type="text" name="s" class="search-input" placeholder="Search Here" autocomplete="off" />
		<input type="submit" value="search" />
	</form>
</div>

<div class="fb-likebox">
	<div class="fb-page" data-href="https://www.facebook.com/maxTelcomteam/" data-tabs="false" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/maxTelcomteam/"><a href="https://www.facebook.com/maxTelcomteam/">MaxTelcom Team</a></blockquote></div></div>
</div>

<div class="widget-container">
	<?php if (is_active_sidebar('blog-sidebar')): ?>
		<ul><?php dynamic_sidebar('blog-sidebar'); ?></ul>
	<?php endif; ?>
</div>