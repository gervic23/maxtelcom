<?php get_header(); ?>
<div class="wp-content-container">
	<section class="mainSection">
		<div class="singleMainContents">
			<?php if (have_posts()): ?>
				<?php while (have_posts()): the_post(); ?>
					<div class="featImage"><?php if (has_post_thumbnail()): ?><?php the_post_thumbnail(); ?><?php else: ?><img alt="default-feat-image" src="/wp-content/themes/maxtelcom/img/maxtelcom-large.jpg" /><?php endif; ?></div>
					<div class="singleContainer">
						<div class="singleTitle"><h1><?php the_title(); ?></h1></div>
						<div class="singleContents">
							<div class="singleDate"><?php the_time('F j Y g:i:a') ?></div>
							<div class="singleAuthor">By <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php the_author(); ?></a></div>
							<div class="articleSocialMediaButtons">
							<?php the_content(); ?>
						</div>
						<div class="fblike">
							<div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
						</div>
						<div class="mobile-fblike">
							<div class="mobile-fblike fb-like" data-href="<?php the_permalink(); ?>" data-layout="box_count" data-action="like" data-show-faces="true" data-share="true"></div>
						</div>
						<div class="articleComments">
							<?php comments_template('', true); ?>
						</div>
					</div>
				<?php endwhile; ?>
			<?php else: ?>
				&nbsp;
			<?php endif; ?>
		</div>
	</section>
	<aside class="mainAside">
		<?php require_once('includes/side.php'); ?>
	</aside>
	<div class="clear"></div>
</div>

<script type="text/javascript">
	var $ = jQuery;

	winresize();

	$(window).resize(function() {
		winresize();
	});

	function winresize() {
		if ($(window).width() > 800) {
			var h = $(window).height() - 120;
		} else if ($(window).width() <= 800) {
			var h = $(window).height() - 60;
		}

		var h2 = $(window).height() - 140;
		var h3 = $(window).height();
		var h4 = h3 - 250;
		var mainSection_width = $(".mainSection").width();

		$("body").css({
			"background-image": "url(/wp-content/themes/maxtelcom/img/blog-bg.jpg)",
			"background-repeat": "no-repeat",
			"background-size": $(window).width() + "px " + $(window).height() + "px",
			"background-attachment": "fixed"
		});

		$(".wp-content-container").css({
			"height": h + "px",
			"overflow-y": "hidden"
		});

		$(".singleMainContents").css({
			"height": h + "px",
			"width": mainSection_width + "px",
		});

		$(".featImage").css({
			"width": mainSection_width + "px",
			"height": h2 + "px"
		});

		$(".singleContainer").css({
			"margin-top": h3 + "px",
		});

		$(".singleMainContents").slimscroll({
			height: h + "px",
			color: "#525354",
			size: "10px",
			alwaysVisible: true
		});

		setTimeout(function() {
			$(".singleContainer").animate({
				marginTop: h4 + "px"
			}, 500);
		}, 4000);
	}

	function setCookie(page) {
		var d = new Date();
		d.setTime(d.getTime() + (10 * 1000));
		var expires = "expires=" + d.toUTCString();
		document.cookie = "page" + "=" + page + "; " + expires + ";" + "path=/";

		location = "/";
	}
</script>

<?php get_footer(); ?>